/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Agencia;
import modelo.enums.TipoMes;

/**
 *
 * @author Usuario
 */
public class ventasController {

    private Agencia agencias[];
    private Double ventas[][];
    private Agencia agencia;

    public ventasController() {
        agencias = new Agencia[5];
        ventas = new Double[5][TipoMes.values().length];
    }

    public Agencia[] getAgencias() {
        return agencias;
    }

    public void setAgencias(Agencia[] agencias) {
        this.agencias = agencias;
    }

    public Double[][] getVentas() {
        return ventas;
    }

    public void setVentas(Double[][] ventas) {
        this.ventas = ventas;
    }

    public Agencia getAgencia() {
        if (agencia == null) {
            this.agencia = new Agencia();
        }
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public boolean ingresarAgencia() {
        int pos = verificardis();
        if (pos >= 0) {
            this.agencias[pos] = this.getAgencia();
            return true;
        } else {
            return false;
        }
    }

    private int verificardis() {
        int pos = -1;
        for (int i = 0; i < this.agencias.length; i++) {
            if (this.agencias[i] == null) {
                pos = i;
                break;
            }
        }
        return pos;
    }

    public Agencia[] inicializar() {
        Agencia[] aux = new Agencia[1];
        return aux;
    }

    public String[] EnumerarMeses() {
        String[] aux = new String[TipoMes.values().length];
        int cont = 0;
        for (TipoMes tp : TipoMes.values()) {
            aux[cont] = tp.toString();
            cont++;
        }

        return aux;
    }

    public boolean ingresarVenta(int pos_persona, int mes, Double valor) {
        try {
            this.ventas[pos_persona][mes] = valor;
            return true;
        } catch (Exception e) {
            System.out.println("error" + e);
        }
        return false;
    }

    public String[][] generarTabla() {
        if (agencias != null && ventas != null) {
            String[][] lista = new String[agencias.length][ventas[0].length + 1];
            for (int i = 0; i < agencias.length; i++) {
                if (agencias[i] != null) {
                    lista[i][0] = agencias[i].toString();
                }
            }
            for (int i = 0; i < ventas.length; i++) {
                for (int j = 0; j < ventas[0].length; j++) {
                    if (ventas[i][j] != null) {
                        lista[i][j + 1] = String.valueOf(ventas[i][j]);
                    } else {
                        lista[i][j + 1] = "0";
                    }
                }
            }
            return lista;
        } else {
            return null;
        }

    }

    public double VentaTotal(Double[] ventasmes) {
        Double suma = 0.0;
        for (int i = 0; i < ventasmes.length; i++) {
            if (ventasmes[i] != null) {
                suma = suma + ventasmes[i];
            }
        }
        return suma;
    }

    public double VentaPromedioMes(Double[] ventas_mes) {
        Double suma = 0.0;
        int cont = 0;
        for (int i = 0; i < ventas_mes.length; i++) {
            if (ventas_mes[i] != null) {
                suma = suma + ventas_mes[i];
                cont++;
            }
        }
        return suma / cont;
    }
    
     

    public int numeroMayorPosicion(Double[] ventas_mes) {
        Double mayor = ventas_mes[0];
        int posicion = 0;
        for (int i = 0; i < ventas_mes.length; i++) {
            if(ventas_mes[i] > mayor){
                mayor = ventas_mes[i];
                posicion = i;
            }
        }
        return posicion;
    }
    
    public int numeroMenorPosicion(Double[] ventas_mes) {
        Double menor = ventas_mes[0];
        int posicion = 0;
        for (int i = 0; i < ventas_mes.length; i++) {
            if(ventas_mes[i] < menor){
                menor = ventas_mes[i];
                posicion = i;
            }
        }
        return posicion;
    }

}
