package Vista;

import Controlador.ventasController;
import Vista.modelo.TablaModeloAgencia;
import java.util.Arrays;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Usuario
 */
public class FrmAgencia extends javax.swing.JDialog {

    /**
     * Creates new form FrmAgencia
     */
    private ventasController vc = new ventasController();
    ;
    private TablaModeloAgencia mo = new TablaModeloAgencia();

    public FrmAgencia(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.cargarListaAgencia();
        this.cbmeses.removeAllItems();
        cargarMeses();
        cargartabla();
    }

    private void cargar() {

    }

    private void cargarMeses() {
        for (int i = 0; i < vc.EnumerarMeses().length; i++) {
            cbmeses.addItem(vc.EnumerarMeses()[i]);
        }
    }

    private String[] cargarAgencias() {
        String[] aux = new String[5];
        for (int i = 0; i < vc.getAgencias().length; i++) {
            aux[i] = Arrays.toString(vc.getAgencias());
        }
        return aux;
    }

    private void cargarListaAgencia() {
        listve.setListData(cargarAgencias());
        listve.updateUI();
    }

    private void SeleccionarAgencia() {
        int fila = listve.getSelectedIndex();
        if (fila >= 0 && vc != null & vc.getAgencias()[fila] != null) {
            txAgencia1.setText(vc.getAgencias()[fila].getNombre());
        } else {
            txAgencia1.setText("No hay información");
        }
    }

    private void cargartabla() {
        mo.setVentas(vc.generarTabla());
        mo.setNombreColumnas(vc.EnumerarMeses());
        tabla.setModel(mo);
        tabla.updateUI();
    }

    private void guardarventas() {
        try {
            int fila = listve.getSelectedIndex();
            if (vc != null & vc.getAgencias()[fila] != null) {
                if (vc.ingresarVenta(fila, cbmeses.getSelectedIndex(), Double.parseDouble(txValor.getText()))) {
                    JOptionPane.showMessageDialog(null, "Operacion correcta");
                    txValor.setText(" ");
                    cargartabla();
                } else {
                    JOptionPane.showMessageDialog(null, "error ", "No se pudo guardar", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "error ", "Escoga un cliente", JOptionPane.ERROR_MESSAGE);
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Error", e.toString(), JOptionPane.ERROR_MESSAGE);
        }
    }

    private void VentaTotalanio() {
          int fila = tabla.getSelectedRow();
          Double[] aux = new Double[12];
          if(fila<0){
              JOptionPane.showMessageDialog(null, "Escoga una fila de agencia ", "Error", JOptionPane.ERROR_MESSAGE);
          }else{
              for (int i = 1; i <= 12; i++) {
                  aux[i-1] = Double.parseDouble((String) tabla.getValueAt(fila, i));                   
              }
              Double total = vc.VentaTotal(aux);
              JOptionPane.showMessageDialog(null, "El total de ventas de la Agencia "+tabla.getValueAt(fila, 0)+" es: "+total, "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
          }
    }
    
    private void PromedioVentasMes() {
          int colum = tabla.getSelectedColumn();
          Double[] aux = new Double[12];
          if(colum<1){
              JOptionPane.showMessageDialog(null, "Escoga una columna de mes ", "Error", JOptionPane.ERROR_MESSAGE);
          }else{
              for (int i = 0; i <= 4; i++) {
                  aux[i] = Double.parseDouble((String) tabla.getValueAt(i, colum));
              }
              Double total = vc.VentaPromedioMes(aux);
              JOptionPane.showMessageDialog(null, "El promedio de ventas del mes: "+vc.EnumerarMeses()[colum-1]+" es: "+total, "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
          }
    }
    
    private void VentaMayorMes() {
          int colum = tabla.getSelectedColumn();
          Double[] aux = new Double[5];
          if(colum<0){
              JOptionPane.showMessageDialog(null, "Escoga una columna de mes ", "Error", JOptionPane.ERROR_MESSAGE);
          }else{
              for (int i = 0; i <= 4; i++) {
                  aux[i] = Double.parseDouble((String) tabla.getValueAt(i, colum));
              }
              int total = vc.numeroMayorPosicion(aux);
              JOptionPane.showMessageDialog(null, "La agencia que tuvo mayores ventas en el mes "+vc.EnumerarMeses()[colum-1]+" es: "+tabla.getValueAt(total, 0), "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
          }
    }
    
    
    private void VentaMenorMes() {
          int colum = tabla.getSelectedColumn();
          Double[] aux = new Double[5];
          if(colum<0){
              JOptionPane.showMessageDialog(null, "Escoga una columna de mes ", "Error", JOptionPane.ERROR_MESSAGE);
          }else{
              for (int i = 0; i <= 4; i++) {
                  aux[i] = Double.parseDouble((String) tabla.getValueAt(i, colum));
              }
              int total = vc.numeroMenorPosicion(aux);
              JOptionPane.showMessageDialog(null, "La agencia que tuvo menores ventas en el mes "+vc.EnumerarMeses()[colum-1]+" es: "+tabla.getValueAt(total, 0), "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
          }
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txAgencia1 = new javax.swing.JLabel();
        cbmeses = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txValor = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jButton6 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        txguardar = new javax.swing.JTextField();
        btnguardar = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        listve = new javax.swing.JList<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 153));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "REGISTRO DE VENTAS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 1, 14))); // NOI18N
        jPanel1.setLayout(null);

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabla);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(250, 50, 720, 240);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel1.setText("Valor");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(20, 180, 60, 20);

        txAgencia1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jPanel1.add(txAgencia1);
        txAgencia1.setBounds(80, 50, 120, 30);

        cbmeses.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel1.add(cbmeses);
        cbmeses.setBounds(90, 110, 140, 30);

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel3.setText("Agencia");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(20, 50, 60, 20);

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel4.setText("Mes");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(20, 110, 60, 20);
        jPanel1.add(txValor);
        txValor.setBounds(90, 180, 130, 30);

        jButton1.setBackground(new java.awt.Color(153, 255, 153));
        jButton1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jButton1.setText("Ingresar Ventas");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(30, 220, 130, 30);

        jButton2.setBackground(new java.awt.Color(153, 255, 153));
        jButton2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jButton2.setText("VENTA TOTAL");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(120, 360, 140, 30);

        jButton3.setBackground(new java.awt.Color(153, 255, 153));
        jButton3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jButton3.setText("PROMEDIO DE VENTAS MES");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(280, 360, 200, 30);

        jButton4.setBackground(new java.awt.Color(153, 255, 153));
        jButton4.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jButton4.setText("MES MAYOR EN VENTAS");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4);
        jButton4.setBounds(520, 360, 200, 30);

        jButton5.setBackground(new java.awt.Color(153, 255, 153));
        jButton5.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jButton5.setText("MES CON MENOS VENTAS");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton5);
        jButton5.setBounds(760, 360, 200, 30);
        jPanel1.add(jPanel3);
        jPanel3.setBounds(860, -50, 100, 100);

        jButton6.setBackground(new java.awt.Color(255, 102, 102));
        jButton6.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton6.setText("Salir");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton6);
        jButton6.setBounds(910, 10, 61, 25);

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 230, 990, 590));

        jPanel4.setBackground(new java.awt.Color(255, 255, 153));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel5.setText("MIRASOL");
        jPanel4.add(jLabel5);

        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 40));

        jPanel2.setBackground(new java.awt.Color(102, 255, 102));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DATOS AGENCIA", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 1, 14))); // NOI18N
        jPanel2.setLayout(null);
        jPanel2.add(txguardar);
        txguardar.setBounds(140, 40, 120, 30);

        btnguardar.setBackground(new java.awt.Color(153, 255, 153));
        btnguardar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnguardar.setText("Guardar");
        btnguardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnguardarActionPerformed(evt);
            }
        });
        jPanel2.add(btnguardar);
        btnguardar.setBounds(280, 40, 90, 30);

        listve.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        listve.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listveMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(listve);

        jPanel2.add(jScrollPane3);
        jScrollPane3.setBounds(430, 20, 290, 150);

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel2.setText("Nombre agencia");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(20, 40, 120, 30);

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Modelo/descarga.jpg"))); // NOI18N
        jLabel6.setText("jLabel6");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(750, 20, 230, 150);

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 990, 190));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void guardar() {
        if (txguardar.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Llene todos los campos vacios");
        } else {
            vc.setAgencia(null);
            vc.getAgencia().setNombre(txguardar.getText());
            if (vc.ingresarAgencia()) {
                JOptionPane.showMessageDialog(null, "Se ha guardado correctamente");
                this.cargarListaAgencia();
                txguardar.setText("");
            } else {
                JOptionPane.showMessageDialog(null, "No se pudo guardar");
            }
        }
    }

    private void btnguardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnguardarActionPerformed
        // TODO add your handling code here:
        guardar();
    }//GEN-LAST:event_btnguardarActionPerformed

    private void listveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listveMouseClicked
        SeleccionarAgencia();
    }//GEN-LAST:event_listveMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        guardarventas();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        VentaTotalanio();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        this.PromedioVentasMes();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        this.VentaMayorMes();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        this.VentaMenorMes();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jButton6ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmAgencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmAgencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmAgencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmAgencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmAgencia dialog = new FrmAgencia(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnguardar;
    private javax.swing.JComboBox<String> cbmeses;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JList<String> listve;
    private javax.swing.JTable tabla;
    private javax.swing.JLabel txAgencia1;
    private javax.swing.JTextField txValor;
    private javax.swing.JTextField txguardar;
    // End of variables declaration//GEN-END:variables
}
