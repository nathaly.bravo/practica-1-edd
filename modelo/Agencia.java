/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import modelo.enums.TipoMes;
/**
 *
 * @author Usuario
 */
public class Agencia {
    private int id;
    private String nombre;
    private Double[] ventas_mes;

    public Double[] getVentas_mes() {
        if(this.ventas_mes == null)
            ventas_mes = new Double[TipoMes.values().length];
        return ventas_mes;
    }
    
    public void setVentas_mes(Double[] ventas_mes) {
        this.ventas_mes = ventas_mes;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }
    
   
}
