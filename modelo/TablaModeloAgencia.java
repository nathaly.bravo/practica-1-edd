/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista.modelo;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Usuario
 */
public class TablaModeloAgencia extends AbstractTableModel {
    private  String[][] ventas;
    private String NombreColumnas[];

    public String[][] getVentas() {
        return ventas;
    }

    public void setVentas(String[][] ventas) {
        this.ventas = ventas;
    }

    public String[] getNombreColumnas() {
        return NombreColumnas;
    }

    public void setNombreColumnas(String[] NombreColumnas) {
        this.NombreColumnas = NombreColumnas;
    }

    
    
    @Override
    public int getRowCount() {
        return ventas.length;
    }

    @Override
    public int getColumnCount() {
       return NombreColumnas.length+1;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        String aux = ventas[i][i1];
        return aux;
    }
    
    @Override
    public String getColumnName(int column) {
        if(column == 0){
            return "Agencia";
        }
        else{
            return NombreColumnas[column-1];
        }
    }
}
